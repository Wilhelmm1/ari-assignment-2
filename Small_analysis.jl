### A Pluto.jl notebook ###
# v0.19.2

using Markdown
using InteractiveUtils

# ╔═╡ ba96c670-c7ee-11ec-18fd-31164c82ccab
using DataStructures

# ╔═╡ 0baab7f2-3912-4329-b0b5-621e9fe6d8b7
using Crayons

# ╔═╡ db083e35-f8da-490a-ae32-4ad7d443205d
mutable struct Building
	floor::Int64
	office::Int64
end

# ╔═╡ ecf29232-c166-4588-b775-61732a456d75
struct State
	hasparcels::Matrix{Int64}
	hasagent::Building
end

# ╔═╡ 4f99438f-b05c-47a0-aebc-f102d3e45780
@enum Action me mw mu md co

# ╔═╡ 9a20f560-c8ec-49b3-824d-1f77bc22d164
begin
	max_iterations = 100
	numberOfFloors = 4
	officesPerFloor = 5
	parcels = zeros(Int64, numberOfFloors, officesPerFloor)
	parcels[1,1] = 1
	parcels[1,2] = 5
	parcels[1,3] = 4
	parcels[1,4] = 1
	parcels[1,5] = 1
	parcels[2,1] = 3
	parcels[2,2] = 2
	parcels[2,4] = 2
	parcels[3,4] = 2
	parcels[4,5] = 1
	parcels[4,3] = 3
end

# ╔═╡ cf108702-6bf9-4428-8936-04813ae63905
initial_state = State(parcels, Building(1,1))

# ╔═╡ 1ec46056-2e73-45b4-966f-47bc934caeed
function officeNumber(floor, office_position)
	if office_position > officesPerFloor
		throw("There is no such room $office_position on floor")
	end
	if floor > numberOfFloors
		throw("There is no such floor")
	end
	return ((floor - 1) * officesPerFloor) + office_position
end

# ╔═╡ ffe06ef4-2d48-4b81-8f2e-fc18dc8fb83d
action = Dict{Action, Int64}(
	mw => 2,
	me => 2,
	mu => 1,
	md => 1,
	co => 5
)

# ╔═╡ 849d8eca-ee74-4608-89cb-f767f026a0c3
action_meaning = Dict{Action, String}(
	me => "Move East",
	mw => "Move West",
	mu => "Move Up",
	md => "Move Down",
	co => "Collect parcel"
)


# ╔═╡ bfee71a2-c643-40b7-bdd3-fc24b854d453
mutable struct Node
	state::State
	parent::Union{Nothing, Node}
	action::Union{Nothing, Action}
	path_cost::Int8
	final_score::Int
end

# ╔═╡ f4d262bc-30e6-44a0-9efe-987eda40a1c7
function transition_model(state, parent, action)
	h = sum(state.hasparcels) * 3
	g = parent.path_cost + action_meaning[action]
	return Node(state, parent, action, g, g + h)
end

# ╔═╡ 75eedfa8-484c-4ae2-b355-30a54c4b1092
function n_transitions(parent::Node)
	state = parent.state
	# creating an empty transition array that will store the history of the agent as it transvers throught its environment
	transitions::Array{Node} = []

	# As the agent move east
	if state.hasagent.office < officesPerFloor
		new_state = deepcopy(state)
		new_state.hasagent.office += 1
		push!(transitions, transition_model(new_state, parent, me))
	end
	
	# As the agent move west
	if state.hasagent.office > 1
		new_state = deepcopy(state)
		new_state.hasagent.office -= 1
		push!(transitions, transition_model(new_state, parent, mw))
	end
	
	# MAs the agent move up
	if state.hasagent.floor < num_floors
		new_state = deepcopy(state)
		new_state.hasagent.floor += 1
		push!(transitions, transition_model(new_state, parent, mu))
	end
	
	# As the agent move down
	if state.hasagent.floor > 1
		new_state = deepcopy(state)
		new_state.hasagent.floor -= 1
		push!(transitions, transition_model(new_state, parent, md))
	end
	
	# As the agent collect the parcel
	if state.hasparcels[state.hasagent.floor, state.hasagent.office] > 0
		new_state = deepcopy(state)
		new_state.hasparcels[state.hasagent.floor, state.hasagent.office] -= 1
		push!(transitions, transition_model(new_state, parent, co))
	end

	return transitions
end

# ╔═╡ 8a82cbf3-d94e-4e41-b1fe-b27842cc1f1d
function goal_state(state::State)
	#The goal state can only be achieved if and only if the sum is equivalent to 0
	return sum(state.hasparcels) == 0
end

# ╔═╡ 2f5ea0e6-3646-42c7-87db-47904026051e
function get_path(node::Node)
	path = [node]
	while !isnothing(node.parent)
		node = node.parent
		pushfirst!(path, node)
	end
	return path
end

# ╔═╡ bbd85dec-6947-4147-b8c1-518a1b2f34df
function n_path(path)
	for i in 1:length(path)
		node = path[i]
		output = ""
		if !isnothing(node.action)
			action = action_names[node.action]
			if node.action != co && i < length(path)
				next_node = path[i+1]
				action *= " to floor $(next_node.state.hasagent.floor) office $(next_node.state.hasagent.office) room $(get_room_number(next_node.state.hasagent.floor, next_node.state.hasagent.office))"
			end
			output *= "$(action)"
		end
		for floor in reverse(1:num_floors)
			for office in 1:officesPerFloor
				char = string(node.state.hasparcels[floor, office])
				if node.state.hasagent.floor == floor && node.state.hasagent.office == office
					output *= string(Crayon(foreground = :green))
				end
				output *= "$(char) "
				output *= string(Crayon(foreground = :white))
			end
			output *= "\n"
		end
		println(output)
	end
end

# ╔═╡ 38d28964-7b21-421f-8828-21a9e12d4eb1
function solution(node::Node, explored::Array{Node})
	path = get_path(node)
	actions = []
	rooms = []
	for node in path
		if !isnothing(node.action)
			push!(actions, node.action)
			push!(rooms, get_room_number(node.state.hasagent.floor, node.state.hasagent.office))
		end
	end

	println("Found $(length(actions)) step solution with cost $(node.path_cost) in $(length(explored)) iterations")
	println("Actions: $(join(actions, " -> "))")
	println("Room order: $(join(rooms, " -> "))")
	println("\n===\n")
	visualize_path(path)
end

# ╔═╡ 25d07f77-1cf6-41bb-a986-0f617e9fa6bc
function find_node(node::Node, list)::Union{Nothing, Node}
	for i in list
		if i.state.hasparcels == node.state.hasparcels && i.state.hasagent.floor == node.state.hasagent.floor && i.state.hasagent.office == node.state.hasagent.office
			return i
		end
	end
	return nothing
end

# ╔═╡ 6ef9de60-f525-42dc-a45c-74f1bf611f4a
function node_in_list(node::Node, list)
	return !isnothing(find_node_in_list(node, list))
end


# ╔═╡ eeb7572b-a8af-416a-8004-0e5557f94a1a
function a_star_search(start::State)
	node = Node(start, nothing, nothing, 0, 0)
	frontier = PriorityQueue{Node, Int}()
	enqueue!(frontier, node, node.final_score)
	explored::Array{Node} = []
	iteration = 0
	while !isempty(frontier)
		node = dequeue!(frontier)
		if goal_state(node.state)
			return solution(node, explored)
		end
		push!(explored, node)
		
		for child in n_transitions(node)
			if !node_in_list(child, explored)
				existing = find_node_in_list(child, keys(frontier))
				if isnothing(existing)
					enqueue!(frontier, child, child.final_score)	
				elseif child.path_cost < existing.path_cost
					existing.path_cost = child.path_cost
					existing.parent = child.parent
					existing.action = child.action
				end
			end
		end
		iteration += 1
		if iteration > max_iterations
			return failure("Timed out")
		end
	end
	return failure("No solution has been found")
end

# ╔═╡ ec692c6e-870b-42d6-a841-c8bfe961ef7e
a_star_search(initial_state)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Crayons = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"

[compat]
Crayons = "~4.1.1"
DataStructures = "~0.18.11"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═ba96c670-c7ee-11ec-18fd-31164c82ccab
# ╠═0baab7f2-3912-4329-b0b5-621e9fe6d8b7
# ╠═db083e35-f8da-490a-ae32-4ad7d443205d
# ╠═ecf29232-c166-4588-b775-61732a456d75
# ╠═4f99438f-b05c-47a0-aebc-f102d3e45780
# ╠═9a20f560-c8ec-49b3-824d-1f77bc22d164
# ╠═cf108702-6bf9-4428-8936-04813ae63905
# ╠═1ec46056-2e73-45b4-966f-47bc934caeed
# ╠═ffe06ef4-2d48-4b81-8f2e-fc18dc8fb83d
# ╠═849d8eca-ee74-4608-89cb-f767f026a0c3
# ╠═bfee71a2-c643-40b7-bdd3-fc24b854d453
# ╠═f4d262bc-30e6-44a0-9efe-987eda40a1c7
# ╠═75eedfa8-484c-4ae2-b355-30a54c4b1092
# ╠═8a82cbf3-d94e-4e41-b1fe-b27842cc1f1d
# ╠═2f5ea0e6-3646-42c7-87db-47904026051e
# ╠═bbd85dec-6947-4147-b8c1-518a1b2f34df
# ╠═38d28964-7b21-421f-8828-21a9e12d4eb1
# ╠═25d07f77-1cf6-41bb-a986-0f617e9fa6bc
# ╠═6ef9de60-f525-42dc-a45c-74f1bf611f4a
# ╠═eeb7572b-a8af-416a-8004-0e5557f94a1a
# ╠═ec692c6e-870b-42d6-a841-c8bfe961ef7e
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
